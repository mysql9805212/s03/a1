-- [Section] CREATING/INSERTING Records/Rows
-- Syntax: INSERT INTO <table_name>(columns) VALUES (values);

INSERT INTO artists(name) VALUES ("Rivermaya");

INSERT INTO artists(name) VALUES ("Blackpink");

INSERT INTO artists(name) VALUES ("Taylor Swift");

INSERT INTO artists(name) VALUES ("New Jeans");

INSERT INTO artists(name) VALUES ("Bamboo");

INSERT INTO artists(name) VALUES ("NCT");

INSERT INTO artists(name) VALUES ("SHINee");


INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Born Pink", "2022-09-22", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Midnights", "2022-10-21", 3);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("New Jeans", "2022-08-01", 4);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("As The Music Plays", "2004-02-01", 5);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Tomorrow Becomes Yesterday", "2008-09-26", 5);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("NCT RESONANCE Pt. 1", "2020-10-12", 4);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Universe", "2021-12-14", 4);


INSERT INTO songs(song_name, length, genre, album_id)
	VALUES ("Snow on the Beach", 256, "Pop", 4),
			("Anti-Hero", 201, "Pop", 4);

-- INSERT INTO songs w/o genre

INSERT INTO songs(song_name, length, album_id)
	VALUES ("Bejeweled", 314, 4);

INSERT INTO songs(song_name, album_id)
	VALUES ("Paris", 4); -- Note that MySQL wil still allow this entry to be added even if the property is null. Take caution!

INSERT INTO songs(song_name, length, genre, album_id)
	VALUES ("Masaya", 450, "Jazz", 7),
			("Mr. Clay", 357, "Jazz", 7),
			("Noypi", 700, "OPM", 7);

-- MINI-ACTIVITY: Add 2 songs per album

INSERT INTO songs(song_name, length, genre, album_id)
	VALUES ("Kundiman", 528, "OPM", 1),
			("Is It Sunny Where You Are?", 309, "Rock", 1),
			("Pink Venom", 307, "K-Pop", 2),
			("Yeah Yeah Yeah", 259, "K-Pop", 2),
			("Pretty Savage", 319, "K-Pop", 3),
			("How You Like That", 301, "K-Pop", 3),
			("Make A Wish (Birthday Song)", 350, "K-Pop", 5),
			("Music, Dance", 356, "K-Pop", 5),
			("I Don't Wanna Wait In Vain (For Your Love)", 511, "Rock", 6),
			("Pride and the Flame", 450, "Pop", 6),
			("Universe (Let's Play Ball)", 352, "K-Pop", 8),
			("Beautiful", 422, "K-Pop", 8);

-- [Section] READ data from the database
-- Syntax:
	-- SELECT <column_name> FROM <table_name>;

SELECT * FROM songs; -- returns entire content of songs table

-- Specific columns
SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;

-- Filtering the output of the SEELCT operation
SELECT * FROM songs WHERE genre = "OPM";

-- Find albums produced by Taylor Swift

SELECT * FROM albums WHERE artist_id = 3;

-- We can also use AND and OR keywords for multiple expressions in the WHERE clause.

-- Displaying the title and length of the OPM songs that are more than 4 minutes long

SELECT song_name, length FROM songs WHERE length > 400 AND genre = "OPM";

SELECT song_name, genre FROM songs WHERE genre = "Jazz" OR genre = "OPM";

-- [Section] UPDATING records
-- UPDATE <table_name> SET <column_name> = <value_to_be> WHERE <condition>
UPDATE songs SET length = 428 WHERE song_name = "Kundiman"; -- Just be careful about updating information using the song_name, because ALL songs with the title "Kundiman" will be updated

UPDATE songs SET genre = "Original Pinoy Music" WHERE genre = "OPM";

UPDATE songs SET genre = "Pop" WHERE genre IS NULL;

-- [Section] DELETING records
-- DELETE FROM <table_name> WHERE <condition>;
-- Delete ALL OPM songs that are more than 4 minutes long

DELETE FROM songs WHERE genre = "Original Pinoy Music" AND length > 400;

-- Delete Pop/Other Genre songs that are greater than 5 minutes.
DELETE FROM songs WHERE genre = "Rock" AND length > 500;

DELETE FROM songs WHERE genre = "Pop" AND length = 0;